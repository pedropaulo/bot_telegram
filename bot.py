# -*- coding: utf-8 -*-
import telegram
import configparser
import redis
import os

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

#Configurando bot
# try:
config = configparser.ConfigParser()
config.read_file(open('config.ini'))
# except Exception as e:
# 	print(e)

#Conectando na API do Telegram
# try:
updater = Updater(token = config['DEFAULT']['token'])
dispatcher = updater.dispatcher
# except Exception as e:
# 	print(e)

#Conectado no Banco de dados Redis
db  = redis.StrictRedis(host=config['DB']['host'],
                       port=config['DB']['port'],
                       db=config['DB']['db'])
try:
	def start(bot, update):
	    """
	        Shows an welcome message and help info about the available commands.
	    """
	    me = bot.get_me()

	    # Welcome message
	    msg = "Olá, esse é o bot de testes da ufopa!\n"
	    msg += "I'm {0} and I came here to help you.\n".format(me.first_name)
	    msg += "O que você gostaria que eu fizesse\n\n"
	    msg += "/suporte - comando de suporte\n"
	    msg += "/configurações - comando de configurações\n\n"

	    # Commands menu
	    main_menu_keyboard = [[telegram.KeyboardButton('/support')],
	    					  [telegram.KeyboardButton('/news')],
	                          [telegram.KeyboardButton('/settings')]]
	    reply_kb_markup = telegram.ReplyKeyboardMarkup(main_menu_keyboard,
	                                                   resize_keyboard=True,
	                                                   one_time_keyboard=True)

	    # Send the message with menu
	    bot.send_message(chat_id=update.message.chat_id,
	                     text=msg,
	                     reply_markup=reply_kb_markup)
except Exception as e:
	print(e)
try:	
	start_handler = CommandHandler('start', start)
	dispatcher.add_handler(start_handler)
except Exception as e:
	print(e)

def support(bot, update):
    """
        Sends the support message. Some kind of "How can I help you?".
    """
    bot.send_message(chat_id=update.message.chat_id,
                     text="Comando de teste de suporte :)")


support_handler = CommandHandler('support', support)
dispatcher.add_handler(support_handler)

def news(bot, update):

	try:
		os.system('rm ufopa.txt')
		os.system('scrapy crawl ufopa')
	except Exception as e:
		print(e)
	file = open("ufopa.txt", "r")
	materia = file.read()
	bot.send_message(chat_id=update.message.chat_id,text="Notícia: %s"%materia)


support_handler = CommandHandler('news', news)
dispatcher.add_handler(support_handler)

def unknown(bot, update):
    """
        Placeholder command when the user sends an unknown command.
    """
    msg = "Desculpe, acho que esse comando não existe!"
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg)

unknown_handler = MessageHandler([Filters.command], unknown)
dispatcher.add_handler(unknown_handler)

