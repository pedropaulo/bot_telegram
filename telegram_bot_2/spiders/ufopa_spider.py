# -*- coding: utf-8 -*-
import scrapy

class QuotesSpider(scrapy.Spider):
    name = "ufopa"
    start_urls = [
        'http://www.ufopa.edu.br/ufopa/comunica/noticias/',
    ]

    def parse(self,response):
    	link = response.css('a.feed-post-link.gui-image-hover.gui-color-primary-link.text-default::attr(href)').extract_first()
    	title = response.css('h3.feed-post-body-title.gui-color-primary::text').extract_first()
    	link = 'http://www.ufopa.edu.br/ufopa/comunica/noticias' + str(link)

    	print("Título: %s" % title)
    	print("Link: %s" % link)

    	with open('ufopa.txt', 'a') as file:
    			file.write("%s \n" %str(title))
    			file.write(str(link))